


#include "SnakeBase.h"
#include "SnakeElementBase.h"


ASnakeBase::ASnakeBase()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;


	
	//LastMoveDirection = EMovementDirection::E_UP;
	
}


void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	
	
}


void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}



void ASnakeBase::AddSnakeElement(int ElementsNum)
{	
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		SnakeElements.Add(NewSnakeElem);
	}
	
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{

	case EMovementDirection::E_UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::E_DOWN:
		MovementVector.X -= ElementSize;
		break;

	case EMovementDirection::E_RIGHT:
		MovementVector.Y += ElementSize;
		break;

	case EMovementDirection::E_LEFT:
		MovementVector.Y -= ElementSize;
		break;
	}

			

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{

		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];

		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);	
}



