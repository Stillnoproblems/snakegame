


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
APlayerPawnBase::APlayerPawnBase()
{
 	
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}


void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	
}


void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	

	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (Value > 1)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::E_UP;
		}
		else if (Value < 1)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::E_DOWN;
		}
	}

}

void APlayerPawnBase::HandlePlayerHorizontalInput(float Value)
{
	if (IsValid(SnakeActor))
	{
		if (Value > 1)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::E_RIGHT;
		}
		else if (Value < 1)
		{
			SnakeActor->LastMoveDirection = EMovementDirection::E_LEFT;
		}



	}


}

